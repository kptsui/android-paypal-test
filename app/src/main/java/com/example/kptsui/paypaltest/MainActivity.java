package com.example.kptsui.paypaltest;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import java.math.BigDecimal;

public class MainActivity extends AppCompatActivity {

    public final static String TAG = "MainActivity";
    public final static int PAYPAL_REQUEST_CODE = 999;
    // Test pay 10 dollars
    public final static int PAY = 10;
    // payment to this account
    public final static String paypal_client_id = "ASnyoDP9KsZ8yVdYCrRaSddOpMQ1utYPHE0GvbkdnD50eAihws3yspNgaMsuhDdw4P5rlsaduNodNszN";

    PayPalConfiguration paypal_config;
    Intent service;

    Button btnPay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnPay = (Button) findViewById(R.id.btnPay);

        // Sandbox for test, Production for real
        paypal_config = new PayPalConfiguration()
                .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
                .clientId(paypal_client_id);

        service = new Intent(this, PayPalService.class);
        service.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, paypal_config);
        startService(service); // listening
    }

    public void pay(View v){
        PayPalPayment payment = new PayPalPayment(new BigDecimal(PAY), "HKD", "Test payment",
                PayPalPayment.PAYMENT_INTENT_SALE);
        // go to PaymentActivity
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, paypal_config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        startActivityForResult(intent, PAYPAL_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PAYPAL_REQUEST_CODE){
            if(resultCode == Activity.RESULT_OK){
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                if(confirm != null){
                    String state = confirm.getProofOfPayment().getState();
                    if(state.equals("approved")){
                        Toast.makeText(this, "Payment is approved", Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "Payment is approved");
                    }
                    else {
                        Toast.makeText(this, "Payment error: " + state, Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "Payment error: " + state);
                    }
                }
                else {
                    Toast.makeText(this, "Confirmation is null", Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "Confirmation is null");
                }
            }
        }
    }
}
